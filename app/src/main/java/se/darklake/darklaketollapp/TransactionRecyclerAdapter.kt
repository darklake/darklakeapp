package se.darklake.darklaketollapp

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.transfer_cell.view.*
import java.text.SimpleDateFormat
import java.util.*

class TransactionRecyclerAdapter(private var _transactions: ArrayList<MoneyTransaction>, private val _context: Context) :
    RecyclerView.Adapter<TransactionsHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TransactionsHolder {
        return TransactionsHolder(LayoutInflater.from(this._context).inflate(R.layout.transfer_cell, p0, false))
    }

    override fun getItemCount(): Int {
        return this._transactions.size
    }

    override fun onBindViewHolder(p0: TransactionsHolder, p1: Int) {

        var deposit = "+ "
        if (this._transactions[p1].Deposit == false) {
            deposit = "- "
        }
        p0._tempAmountText.text = deposit + this._transactions[p1].cost.toString()
        val sfd = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        p0._tempDateText.text = sfd.format(Date(_transactions[p1].time.seconds * 1000))

    }
}

class TransactionsHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
    val _tempAmountText: TextView = v.amount_text_cell
    val _tempDateText: TextView = v.date_text_cell

    init {
        v.setOnClickListener(this)
    }

    override fun onClick(v: View) { }

}
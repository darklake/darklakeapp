package se.darklake.darklaketollapp

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_balance.*
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private lateinit var mMap: GoogleMap
    private var _latitude = 0.0
    private var _longitude = 0.0
    private var _dateString = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)


        //added for menu
        setSupportActionBar(toolbar_map)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        var toggle = ActionBarDrawerToggle(
            this,
            drawer_layout_map,
            toolbar_map,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout_map!!.addDrawerListener(toggle)
        toggle.syncState()
        nav_view_map.setNavigationItemSelectedListener(this)

//added for menu


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        val manager = DBManager()

        this._latitude = intent.getDoubleExtra("latitude", 0.0)
        this._longitude = intent.getDoubleExtra("longitude", 0.0)
        this._dateString = intent.getStringExtra("dateString")


        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val position = LatLng(this._latitude, this._longitude)
        mMap.addMarker(MarkerOptions().position(position).title(this._dateString))
        mMap.moveCamera(CameraUpdateFactory.zoomTo(10f))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position))
    }

    //added for menu
    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.nav_home -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_balance -> {
                val intent = Intent(this, BalanceActivity::class.java)
                startActivity(intent)

            }
            else -> { }

        }
        drawer_layout_balance?.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onResume() {
        if (FirebaseAuth.getInstance().currentUser == null) {
            finish()
            val intent = Intent(this, LoginActivity::class.java)
            startActivityForResult(intent, 2)
        }
        super.onResume()
    }

    fun logoutFromTheApplication_map(view: View) {
        var user = FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent, 2)
    }
//added for menu

}

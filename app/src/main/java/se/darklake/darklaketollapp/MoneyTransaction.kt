package se.darklake.darklaketollapp

import com.google.firebase.Timestamp

class MoneyTransaction(val map: MutableMap<String, Any>) {
    val cost = map["Amount"]
    val time = map["Date"] as Timestamp
    val Deposit = map["Deposit"]
}
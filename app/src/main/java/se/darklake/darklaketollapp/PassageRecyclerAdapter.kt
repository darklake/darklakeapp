package se.darklake.darklaketollapp

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.passage_cell.view.*
import java.text.SimpleDateFormat
import java.util.*

class PassageRecyclerAdapter(private var passages: ArrayList<Passage>, private val context: Context) :
    RecyclerView.Adapter<PassageHolder>() {
    val _context = context
    val _passages = passages

    override fun onBindViewHolder(p0: PassageHolder, p1: Int) {
        p0.tempPositionText?.text = passages[p1].position.toString()
        p0.tempCostText?.text = passages[p1].cost.toString() + " kr"
        var sfd = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        p0.tempTimeText?.text = sfd.format(Date(passages[p1].time.seconds * 1000))

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PassageHolder {
        return PassageHolder(LayoutInflater.from(context).inflate(R.layout.passage_cell, p0, false), _context, _passages)
    }


    override fun getItemCount(): Int {
        return passages.size
    }

    fun update(modelList: ArrayList<Passage>) {
        passages = modelList
        notifyDataSetChanged()
    }
}

class PassageHolder(v: View, context: Context, passages: ArrayList<Passage>) : RecyclerView.ViewHolder(v) {
    val tempPositionText = v.temp_position_text
    val tempCostText = v.temp_cost_text
    val tempTimeText = v.temp_time_text
    val _context = context
    val _passages = passages
}

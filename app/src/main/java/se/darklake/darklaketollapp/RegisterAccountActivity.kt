package se.darklake.darklaketollapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_register_account.*

class RegisterAccountActivity : AppCompatActivity() {
    //This view is use both for RegisterAccountActivity and register account - at least for now
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_account)
    }


    fun onCreateAccountClick(view: View) {
        val displayName: String = input_name.text.toString()
        val email: String = input_email.text.toString()
        val password: String = input_password.text.toString()
        val repeatedPassword = input_repeated_password.text.toString()

        when {
            password != repeatedPassword -> Toast.makeText(
                this@RegisterAccountActivity,
                getString(R.string.passwords_do_not_match),
                Toast.LENGTH_LONG
            ).show()
            displayName == "" -> Toast.makeText(
                this@RegisterAccountActivity,
                getString(R.string.missing_displayname),
                Toast.LENGTH_LONG
            ).show()
            else -> {
                val userAccountManager = UserAccountManager()

                userAccountManager.registerNewUser(
                    email,
                    password,
                    displayName,
                    object : UserAccountManager.UserRegistrationInterface {
                        override fun handleSuccessfulRegistration(firebaseUser: FirebaseUser) {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                getString(R.string.account_created) + " " + firebaseUser.displayName,
                                Toast.LENGTH_LONG
                            ).show()
                            finish()
                        }

                        override fun handleFailedRegistration(errorMessage: String) {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                getString(R.string.account_not_created) + errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                        }

                    })
            }
        }

    }
}

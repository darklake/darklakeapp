package se.darklake.darklaketollapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseUser

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun onLoginButtonClick(view: View) {
        val email: String = findViewById<EditText>(R.id.input_email).text.toString()
        val password: String = findViewById<EditText>(R.id.input_password).text.toString()

        val userAccountManager = UserAccountManager()
        userAccountManager.signInWithEmailAndPassword(email, password, object : UserAccountManager.UserSignInInterface {
            override fun handleSuccessfulSignIn(firebaseUser: FirebaseUser) {
                Toast.makeText(this@LoginActivity, "Successfully signed in", Toast.LENGTH_LONG).show()
                Toast.makeText(this@LoginActivity, firebaseUser.displayName, Toast.LENGTH_LONG).show()
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun handleFailedSignIn(errorMessage: String) {
                Toast.makeText(this@LoginActivity, "Could not sign in " + errorMessage, Toast.LENGTH_LONG).show()
            }

        })
    }

    fun onRegisterButtonClick(view: View) {

        val intent = Intent(this, RegisterAccountActivity::class.java)
        startActivityForResult(intent, 1)
/*
        val intent = Intent(this, RegisterAccountActivity::class.java)
        startActivity(intent)*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
            }
        }
    }

}

package se.darklake.darklaketollapp

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class DarkLakeBluetoothManager(
    bluetoothAdapter: BluetoothAdapter,
    context: Context,
    gantryPassageNotifications: GantryPassageNotificationsInterface
) {
    interface GantryPassageNotificationsInterface {
        fun NotifyRegisteredPassage()
    }

    private val _bluetoothAdapter = bluetoothAdapter
    private val _db = FirebaseFirestore.getInstance()
    private val _uid = FirebaseAuth.getInstance().uid

    private val _filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
    private val _context = context

    private var _broadcastReciever = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent!!.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (device.name == "DarkLake") {
                    val reference = _db.collection("gantryPassages").document(device.address)
                    _db.runTransaction { transaction ->
                        val snapshot = transaction.get(reference)
                        val newCounter = snapshot.getDouble("counter")!! + 1
                        transaction.update(reference, "counter", newCounter)
                        transaction.update(reference, "uId", _uid)
                    }
                    gantryPassageNotifications.NotifyRegisteredPassage()

                }
            }
        }

    }

    fun startDiscovery() {
        this._context.registerReceiver(_broadcastReciever, _filter)
        this._bluetoothAdapter.startDiscovery()
    }

    fun stopDiscovery() {
        this._context.unregisterReceiver(_broadcastReciever)
    }

}
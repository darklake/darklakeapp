package se.darklake.darklaketollapp

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        aboutText.text = Html.fromHtml(
            "This application was created by the group Dark Lake <br/> as an assignment to Kapsch TrafficCom in the " +
                    "course Agile project. <br/> <br/> &copy;All rights reserved to following group members:<br/> <br/> " +
                    "Jin Asp<br/> Willy Bach<br/> Saga Bergdahl<br/> Viktor Jansson<br/> Jesper Persson<br/> " +
                    "Måns Sandberg<br/> Gabriella Stenlund<br/>",
            Html.FROM_HTML_MODE_LEGACY
        )

        setSupportActionBar(toolbar_about)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout_about,
            toolbar_about,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout_about!!.addDrawerListener(toggle)
        toggle.syncState()
        nav_view_about.setNavigationItemSelectedListener(this)
    }

    fun logoutFromTheApplicationAbout(view: View) {
        var user = FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent, 2)
    }

    fun gotoAboutPageAbout(view: View) {
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.nav_home -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_balance -> {
                val intent = Intent(this, BalanceActivity::class.java)
                startActivity(intent)

            }
            else -> { }

        }
        drawer_layout_about?.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onResume() {
        if (FirebaseAuth.getInstance().currentUser == null) {
            finish()
            val intent = Intent(this, LoginActivity::class.java)
            startActivityForResult(intent, 2)
        }
        super.onResume()
    }
}

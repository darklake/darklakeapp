package se.darklake.darklaketollapp

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_balance.*

class BalanceActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    //added for menu
    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.nav_home -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_balance -> {
                val intent = Intent(this, BalanceActivity::class.java)
                startActivity(intent)

            }
            else -> {  }

        }
        drawer_layout_balance?.closeDrawer(GravityCompat.START)
        return true
    }
//added for menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_balance)

//added for menu
        setSupportActionBar(toolbar_balance)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout_balance,
            toolbar_balance,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout_balance!!.addDrawerListener(toggle)
        toggle.syncState()
        nav_view_balance.setNavigationItemSelectedListener(this)

//added for menu

        val manager = DBManager()
        manager.getBalanceAmount(object : DBManager.balanceAmountResponse {
            override fun getBalanceAmount(amount: Any?) {
                total_amount.text = amount.toString() + " kr"

            }
        })
        val listOfTrans: ArrayList<MoneyTransaction> = ArrayList<MoneyTransaction>()
        val tempRecyclerAdapter = TransactionRecyclerAdapter(listOfTrans, this@BalanceActivity)
        transactionsView.adapter = tempRecyclerAdapter
        transactionsView.layoutManager = LinearLayoutManager(this@BalanceActivity)
        transactionsView.addItemDecoration(DividerItemDecoration(this@BalanceActivity, DividerItemDecoration.VERTICAL))
        tempRecyclerAdapter.notifyDataSetChanged()
        manager.addListenerOnTransactions(listOfTrans, tempRecyclerAdapter)


    }

    fun depositMoneyIntoAccount(view: View) {

        val amountToDeposit: String = findViewById<EditText>(R.id.input_transfer_money).text.toString()
        findViewById<EditText>(R.id.input_transfer_money).text.clear()
        var amountToDepositInt = 0
        try {
            amountToDepositInt = amountToDeposit.toInt()
            if (amountToDepositInt < 0) {
                Toast.makeText(this@BalanceActivity, "Incorrect input", Toast.LENGTH_SHORT).show()
                return
            }

        } catch (e: Exception) {
            Toast.makeText(this@BalanceActivity, "Incorrect input", Toast.LENGTH_SHORT).show()
            return
        }
        val manager = DBManager()
        manager.depositMoney(amountToDepositInt, object : DBManager.transferMoneyResponse {
            override fun transferResponse() {
                manager.getBalanceAmount(object : DBManager.balanceAmountResponse {
                    override fun getBalanceAmount(amount: Any?) {
                        total_amount.text = amount.toString() + " kr"
                        Toast.makeText(
                            this@BalanceActivity,
                            "Deposited " + amountToDepositInt.toString() + " kr",
                            Toast.LENGTH_LONG
                        ).show()

                    }
                })
            }
        })
    }
    //added for menu


    override fun onBackPressed() {
        if (drawer_layout_balance!!.isDrawerOpen(GravityCompat.START)) {
            drawer_layout_balance!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun gotoAboutPageBalance(view: View) {
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    fun logoutFromTheApplicationBalance(view: View) {
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent, 2)
    }

//added for menu
}

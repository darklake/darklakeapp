package se.darklake.darklaketollapp

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import kotlinx.android.synthetic.main.activity_balance.*
import kotlinx.android.synthetic.main.activity_main.*
import se.darklake.darklaketollapp.DarkLakeBluetoothManager.GantryPassageNotificationsInterface

@SuppressLint("ByteOrderMark")
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.nav_home -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_balance -> {
                val intent = Intent(this, BalanceActivity::class.java)
                startActivity(intent)

            }
            else -> {  }

        }
        drawer_layout_balance?.closeDrawer(GravityCompat.START)
        return true
    }

    private var _btManager: DarkLakeBluetoothManager? = null
    private var _passages: ArrayList<Passage> = ArrayList()

    private var notificationManager: NotificationManager? = null
    val passageRecyclerAdapter = PassageRecyclerAdapter(_passages, this@MainActivity)


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout!!.addDrawerListener(toggle)
        toggle.syncState()



        if (ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_COARSE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(ACCESS_COARSE_LOCATION), 5
                )
            }
        }

        this._btManager = DarkLakeBluetoothManager(
            (this.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter,
            this,
            object : GantryPassageNotificationsInterface {
                override fun NotifyRegisteredPassage() {
                    sendNotification("New Passage registered", "Your vehicle has been registered at a toll")
                }

            })

        nav_view.setNavigationItemSelectedListener(this)

        this._btManager!!.startDiscovery()


        val db = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build()
        db.firestoreSettings = settings

        if (savedInstanceState == null) {
            var userDBManager = UserAccountManager()
            val user = FirebaseAuth.getInstance().currentUser
            if (user == null) {
                val intent = Intent(this, LoginActivity::class.java)
                startActivityForResult(intent, 2)
            }
        }
        var map: ArrayList<Passage> = ArrayList()
        val manager = DBManager()
        manager.getPassages(object : DBManager.PassageListResponse {
            override fun getPassageListSuccess(passageList: ArrayList<Passage>?) {
                if (passageList == null || passageList.size == 0) return
                latestPassageName.text = passageList[0].name
                latestPassagePrice.text = passageList[0].cost.toString() + " sek"

            }
        })
        passagesViews.adapter = passageRecyclerAdapter
        passagesViews.layoutManager = LinearLayoutManager(this@MainActivity)
        manager.setChangeListener(this._passages, passageRecyclerAdapter)

        //Notification
        notificationManager =
            getSystemService(
                Context.NOTIFICATION_SERVICE
            ) as NotificationManager

        createNotificationChannel(
            "se.darklake.darklaketollapp.passage",
            "Dark Lake Toll",
            "Toll passage"
        )
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Check which request we're responding to
        if (requestCode == 2) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                var user = FirebaseAuth.getInstance().currentUser

            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout!!.isDrawerOpen(GravityCompat.START)) {
            drawer_layout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun mapButtonPressed(view: View) {
        val parent = view.parent.parent.parent as View
        val recyclerView = parent.parent as RecyclerView
        val index = recyclerView.indexOfChild(parent)
        val passage = _passages[index]
        if (passage.coordinates == null) return
        val intent = Intent(this, MapsActivity::class.java).apply {
            putExtra("latitude", passage.coordinates.latitude)
            putExtra("longitude", passage.coordinates.longitude)
            putExtra("dateString", passage.time.toDate().toString())
        }
        startActivity(intent)
    }

    fun logoutFromTheApplication(view: View) {
        var user = FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent, 2)
    }

    fun gotoAboutPage(view: View) {
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    fun onStartSaldoClick(view: View) {


/*
        val intent = Intent(this, RegisterAccountActivity::class.java)
        startActivity(intent)*/
    }

    //Notification
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(id: String, name: String, description: String) {

        val importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(id, name, importance)

        channel.description = description
        channel.enableLights(true)
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern =
            longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
        notificationManager?.createNotificationChannel(channel)
    }

    override fun onResume() {
        if (FirebaseAuth.getInstance().currentUser == null) {
            finish()
            val intent = Intent(this, LoginActivity::class.java)
            startActivityForResult(intent, 2)
        }
        super.onResume()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun sendNotification(title: String, contentText: String) {

        val notificationID = 101

        val channelID = "se.darklake.darklaketollapp.passage"

        val notification = Notification.Builder(
            this@MainActivity,
            channelID
        )
            .setContentTitle(title)
            .setContentText(contentText)
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setChannelId(channelID)
            .build()

        notificationManager?.notify(notificationID, notification)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this._btManager != null)
            (this._btManager as DarkLakeBluetoothManager).stopDiscovery()
    }
}

package se.darklake.darklaketollapp

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import java.util.*
import com.google.firebase.Timestamp as fireBaseTimeStamp

class UserAccountManager {
    private val _db = FirebaseFirestore.getInstance()

    interface UserSignInInterface {
        fun handleSuccessfulSignIn(firebaseUser: FirebaseUser)
        fun handleFailedSignIn(errorMessage: String)
    }

    interface UserRegistrationInterface {
        fun handleSuccessfulRegistration(firebaseUser: FirebaseUser)
        fun handleFailedRegistration(errorMessage: String)
    }

    private var _auth: FirebaseAuth? = null

    init {
        this._auth = FirebaseAuth.getInstance()
    }

    fun signInWithEmailAndPassword(email: String, password: String, userSignInInterface: UserSignInInterface) {
        if(email == "" || password == ""){
            userSignInInterface.handleFailedSignIn("Email and password are required")
            return
        }
        _auth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener { task: Task<AuthResult> ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                val firebaseUser = this._auth?.currentUser!!
                userSignInInterface.handleSuccessfulSignIn(firebaseUser)
            } else {
                // If sign in fails, display a message to the user.
                if (task.exception != null) {
                    userSignInInterface.handleFailedSignIn(task.exception!!.message.toString())
                } else
                    userSignInInterface.handleFailedSignIn("Something unexpected happened")

            }
        }
    }

    fun registerNewUser(
        email: String,
        password: String,
        displayName: String,
        userRegistrationInterface: UserRegistrationInterface
    ) {
        this._auth?.createUserWithEmailAndPassword(email, password)
            ?.addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    //Registration OK
                    val firebaseUser = this._auth?.currentUser!!
                    val profileUpdates = UserProfileChangeRequest.Builder()
                        .setDisplayName(displayName).build()
                    firebaseUser.updateProfile(profileUpdates)
                        .addOnCompleteListener(OnCompleteListener<Void> { task ->
                            if (task.isSuccessful) {
                                userRegistrationInterface.handleSuccessfulRegistration(firebaseUser)
                            } else {
                                if (task.exception != null) {
                                    userRegistrationInterface.handleFailedRegistration(task.exception!!.message.toString())
                                } else
                                    userRegistrationInterface.handleFailedRegistration("Something unexpected happened")
                            }
                        })
                    this.createNewUser(firebaseUser.uid, displayName)

                } else {
                    //Registration error
                    if (task.exception != null) {
                        userRegistrationInterface.handleFailedRegistration(task.exception!!.message.toString())
                    } else
                        userRegistrationInterface.handleFailedRegistration("Something unexpected happened")
                }
            }
    }

    fun createNewUser(userID: String, name: String) {
        val newUser = HashMap<String, Any>()
        newUser["accumulated_charges"] = 0.0
        newUser["name"] = name
        this._db.collection("users").document(userID)
            .set(newUser)
            .addOnSuccessListener {
                val newPassage = HashMap<String, Any>()
                newPassage["cost"] = 0.0
                newPassage["gId"] = "B8:27:EB:EB:39:17"
                newPassage["time"] = fireBaseTimeStamp.now()
                newPassage["location"] = GeoPoint(0.0, 0.0)
                this._db.collection("users").document(userID).collection("passages")
                    .add(newPassage)
                    .addOnSuccessListener(OnSuccessListener {
                        val newUserBalance = HashMap<String, Any>()
                        newUserBalance["amount"] = 100
                        this._db.collection("bankaccount").document(userID)
                            .set(newUserBalance)
                            .addOnSuccessListener(OnSuccessListener<Void> {

                            })
                    })

            }
            .addOnFailureListener(OnFailureListener {

            })
    }
}
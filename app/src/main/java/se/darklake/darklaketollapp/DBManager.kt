package se.darklake.darklaketollapp

import android.util.Log
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set

class DBManager {
    private val _db = FirebaseFirestore.getInstance()

    interface PassageListResponse {
        fun getPassageListSuccess(passageList: ArrayList<Passage>?)
    }

    interface balanceAmountResponse {
        fun getBalanceAmount(amount: Any?)
    }

    interface transferMoneyResponse {
        fun transferResponse()
    }

    fun setChangeListener(passageList: ArrayList<Passage>?, passageRecyclerAdapter: PassageRecyclerAdapter) {


        val user = FirebaseAuth.getInstance().currentUser ?: return
        _db.collection("users").document(user.uid).collection("passages").orderBy("time", Query.Direction.ASCENDING)
            .addSnapshotListener { value, e ->

                for (dc in value!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            val passage = dc.document.data
                            _db.collection("gantries").document(passage["gId"] as String).get()
                                .addOnSuccessListener { gantry ->
                                    val tempPassage = Passage(passage, gantry.data!!["name"] as String)
                                    if (tempPassage.coordinates != null && (tempPassage.coordinates.latitude != 0.0 || tempPassage.coordinates.longitude != 0.0)) {
                                        passageList!!.add(0, tempPassage)
                                        passageRecyclerAdapter.notifyDataSetChanged()
                                    }
                                }
                        }
                    }
                }

            }
    }

    fun getPassages(response: PassageListResponse) {
        val map: ArrayList<Passage> = ArrayList()
        //when login is implemeneted
        val user = FirebaseAuth.getInstance().currentUser
        if (user == null) {
            response.getPassageListSuccess(null)
            return
        }
        //        _db.collection("users").document("FHwNWh3OvUiq0VmKuMKI").collection("passages")
        _db.collection("users").document(user.uid).collection("passages").orderBy("time", Query.Direction.ASCENDING).get()
            .addOnSuccessListener { passages ->
                _db.collection("gantries").get().addOnSuccessListener { gantries ->
                    for (passage in passages) {
                        for (gantry in gantries) {
                            if (gantry.id == passage.data["gId"]) {
                                val newPassage = Passage(passage.data, gantry.data["name"].toString())
                                if (newPassage.coordinates != null && (newPassage.coordinates.latitude != 0.0 || newPassage.coordinates.longitude != 0.0))
                                    map.add(newPassage)
                            }
                        }

                    }
                    response.getPassageListSuccess(map)

                }
            }
    }

    fun depositMoney(amount: Int, response: transferMoneyResponse) {
        val user = FirebaseAuth.getInstance().currentUser ?: return
        val newTransaction = HashMap<String, Any>()
        newTransaction["Amount"] = amount
        newTransaction["Date"] = Timestamp.now()
        newTransaction["Deposit"] = true

        this._db.collection("bankaccount").document(user.uid).collection("transactions")
            .add(newTransaction)
            .addOnSuccessListener {
                val reference = _db.collection("bankaccount").document(user.uid)
                _db.runTransaction { transaction ->
                    val snapshot = transaction.get(reference)
                    val newAmount = snapshot.getDouble("amount")!! + amount
                    transaction.update(reference, "amount", newAmount)
                }.addOnSuccessListener {
                    response.transferResponse()
                }
            }

    }


    fun getBalanceAmount(response: balanceAmountResponse) {
        val map: ArrayList<Passage> = ArrayList<Passage>()
        //when login is implemeneted
        val user = FirebaseAuth.getInstance().currentUser
        if (user == null) {
            response.getBalanceAmount(null)
            return
        }
        var userId = user.uid
        _db.collection("bankaccount").document(user.uid).get()
            .addOnSuccessListener { balance ->
                var dataFromBank = balance.data
                var amount = dataFromBank!!.get("amount").toString()
                response.getBalanceAmount(amount)
            }
    }

    fun addListenerOnTransactions(transactionsList: ArrayList<MoneyTransaction>?, transAdapter: TransactionRecyclerAdapter) {
        val user = FirebaseAuth.getInstance().currentUser ?: return
        _db.collection("bankaccount").document(user.uid).collection("transactions")
            .orderBy("Date", Query.Direction.ASCENDING)
            .addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    return@EventListener
                }

                for (dc in value!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            transactionsList!!.add(0, MoneyTransaction(dc.document.data))
                            transAdapter.notifyDataSetChanged()
                        }


                    }
                }

            })
    }

}
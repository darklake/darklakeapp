package se.darklake.darklaketollapp

import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint

class Passage(val map: MutableMap<String, Any>, val name: String) {
    val cost = map["cost"]
    val position = name
    val time = map["time"] as Timestamp
    val coordinates = map["location"] as GeoPoint?
}

package se.darklake.darklaketollapp


import android.net.Uri
import com.google.firebase.Timestamp

object TestConstants {
    val EMAIL = "test@example.com"
    val PASSWORD = "hunter2"
    val NAME = "Test Testerson"
    val TOKEN = "token"
    val SECRET = "secret"
    val UID = "uid"
    val SESSION_ID = "sessionId"
    val TOS_URL = "http://www.google.com"
    val PRIVACY_URL = "https://www.google.com/policies/privacy/"
    val PHOTO_URI = Uri.parse("http://example.com/profile.png")
    val COST = 12
    val GANTRYNAME = "E4S-JKPG"
    val TIME = Timestamp(1000000, 1000000)
}
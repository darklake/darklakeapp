package se.darklake.darklaketollapp

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SdkSuppress
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.By
import android.support.test.uiautomator.UiDevice
import android.support.test.uiautomator.UiObject2
import android.support.test.uiautomator.Until
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
class NotificationTest {

    private val LAUNCH_TIMEOUT = 5000L
    @Rule @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }


//    @Test
//    fun clearAllNotifications() {
//        //val uiDevice = getInstance(InstrumentationRegistry.getInstrumentation())
//        uiDevice.openNotification()
//        uiDevice.wait(Until.hasObject(By.textStartsWith(R.string.app_name.toString())), LAUNCH_TIMEOUT)
//        val clearAll: UiObject2 = uiDevice.findObject(By.res(R.string.clear_all_notification.toString()))
//        clearAll.click()
//    }

    @Test
    fun shouldSendNotification() {
        val expectedAppName = "Dark Lake Toll App"
        val expectedTitle = "Registered passage"
        val expectedText = "New passage at " + TestConstants.GANTRYNAME + ", " + TestConstants.TIME + ". Cost: " + TestConstants.COST

        //Expect a notification
        activityRule.activity.sendNotification(expectedTitle, expectedText)

        uiDevice.openNotification()
        uiDevice.wait(Until.hasObject(By.textStartsWith(expectedAppName)), LAUNCH_TIMEOUT)
        val title: UiObject2 = uiDevice.findObject(By.text(expectedTitle))
        val text: UiObject2 = uiDevice.findObject(By.textStartsWith(expectedText))
        assertEquals(expectedTitle, title.text)
        assertTrue(text.text.startsWith(expectedText))
        //clearAllNotifications()
    }

}
package se.darklake.darklaketollapp


import android.content.res.Resources
import android.support.test.espresso.Espresso.closeSoftKeyboard
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.google.firebase.auth.FirebaseUser
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import se.darklake.darklaketollapp.TestConstants.EMAIL
import se.darklake.darklaketollapp.TestConstants.PASSWORD


@RunWith(AndroidJUnit4::class)
class LoginTest{

    fun getMockFirebaseUser(): FirebaseUser {
        val user = mock(FirebaseUser::class.java)
        `when`(user.getUid()).thenReturn(TestConstants.UID)
        `when`(user.getEmail()).thenReturn(TestConstants.EMAIL)
        `when`(user.getDisplayName()).thenReturn(TestConstants.NAME)
        `when`(user.getPhotoUrl()).thenReturn(TestConstants.PHOTO_URI)

        return user
    }

    @Rule @JvmField
    val mActivityRule = ActivityTestRule(LoginActivity::class.java)
    private var resources: Resources? = null

    @Before
    fun init() {
        resources = mActivityRule.activity.resources
    }

    @Test
    fun signInWithEmailAndPassword() {
        //Given
        onView(withId(R.id.input_email)).perform(typeText(EMAIL))
        closeSoftKeyboard()
        onView(withId(R.id.input_password)).perform(typeText(PASSWORD))
        closeSoftKeyboard()
        //When
        onView(withId(R.id.btn_login)).perform(click())

                //TODO: Should probably mock Firebase Auth
        //Then
        //onView(withText(startsWith("Could not sign in "))).inRoot(withDecorView(not(`is`(mActivityRule.activity.window.decorView))))
            .check(matches(isDisplayed()))
    }

}